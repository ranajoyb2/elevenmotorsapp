export interface User {
    AdminUserId: string;
    ApiToken: string;
    Name: string;
    Email: string;
    Phone: string;
    LoginId: string;
    Address: string;
    UserRole: string;
    Status: string;
}
export interface Subject {
    SubjectId: string;
    Name: string;
    CreateBy: string;
    CreateDate: Date;
    ModifyBy: string;
    ModifyDate: Date;
}
