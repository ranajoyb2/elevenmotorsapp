import React from "react-native";

const { StyleSheet } = React;
import BaseColor from './../Core/BaseTheme';

export default {
    SimpleBtnStyle:{ 
              borderRadius: 10,
              width:'80%',
              alignSelf:'center',
              backgroundColor:BaseColor.HeaderColor,
              marginTop:35,
              alignContent:'center',
              alignItems:'center',
              justifyContent:'center',
            },
    AcceptButton:{
              height:40, 
              borderRadius:5,
              marginEnd:10
            },
    RejectButton:{
              height:40,
              borderRadius:5, 
              marginEnd:50
            },
    UploadButton:{
      backgroundColor: BaseColor.HeaderColor,
      justifyContent:'center',
      borderRadius:10
    
    }                        
  
     
    
 
};
