import React from "react-native";

const { StyleSheet } = React;
import BaseColor from './../Core/BaseTheme';

export default {
 LoginIconCard:{
    height:60,
    justifyContent:'center',
    borderRadius:15,
    width:'65%',
    alignItems:'center'
 }
 
};
