import React from "react-native";

const { StyleSheet } = React;
import BaseColor from './../Core/BaseTheme';

export default {

      SimpleBtnTxt: {
            color: BaseColor.ColorWhite
      },
      DashboarditemName: {
            fontSize: 17,
            fontWeight: 'bold',
            alignSelf: 'center',

            color: BaseColor.HeaderColor,
      },
      LoginTxt: {
            marginTop: '17%',
            color: BaseColor.HeaderColor,
            fontWeight: 'bold',
            alignSelf: 'center',
            fontSize: 20
      },
      InputLabelStyle: {
            width: 300,
            color: BaseColor.HeaderColor
      },

      DashboardUserName: {
            margin: 5,
            fontWeight: 'bold',
            fontSize: 20,
            color: BaseColor.ColorWhite
      },
      AddText: {
            marginLeft: 20,
            color: BaseColor.HeaderColor,
            fontWeight: 'bold',
            fontSize: 18,
      },

      ListItemValue: {
            fontSize: 12,
            alignSelf: 'flex-end'
      },
      InputText: {
            marginTop: 5,
            color: BaseColor.HeaderColor,
            fontSize: 18,
      },
      QRcodeHead: {
            alignSelf: 'center',
            margin: 5,
            color: BaseColor.HeaderColor
      },
      QrCodeButton: {
            alignSelf: 'center',
            margin: 5,
            color: BaseColor.HeaderColor,
            fontWeight: 'bold'
      },
      InputValueText: {

            color: BaseColor.HeaderColor

      },
      ListHeader: {
            // marginTop:'5%',
            color: BaseColor.HeaderColor,
            fontSize: 18,
            fontWeight: 'bold',
            fontFamily: 'DancingScript-Bold'
      },
      ListItemHead: {
            fontSize: 12,
            fontWeight: 'bold',
            color: BaseColor.HeaderColor,
            fontFamily: 'DancingScript-Bold'
      },
      
      GridBodyTxt: {
            fontSize: 13,
            fontWeight: '900',
            // alignSelf:'center',
            marginLeft:5
            
          },
          GridBodyTimeslotTxt: {
            fontSize: 11,
            fontWeight: '900',
            alignSelf: 'center',
          },

};
