import React from "react-native";

const { StyleSheet } = React;
import BaseColor from './../Core/BaseTheme';

export default {
  FullScreenView: {
    height: '100%',
    backgroundColor: BaseColor.ColorWhite
  },
  gridView: {
    marginTop: 10,
    flex: 3,
    marginBottom: 10,
  },
  ModalView: {

    backgroundColor: BaseColor.ColorWhite,
    height: '75%',
    marginTop:'30%',
    marginEnd: 20,
    marginLeft: 20,
    borderRadius: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
      marginBottom: 10
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 16
  },

  DashboarditemContainer: {
    alignContent: "center",
    alignItems: 'center',
    backgroundColor: "white",
    justifyContent: 'center',
    borderRadius: 5,
    padding: 10,
    height: 150,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 5
  },
  FullScreenModalView: {
    marginTop: '17%',
    backgroundColor: BaseColor.ColorWhite,
    height: '85%',
    marginEnd: 20,
    marginLeft: 20,
    borderRadius: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 16
  },
  RowView: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  DashboardModalView: {

    backgroundColor: BaseColor.ColorWhite,
    height: '70%',
    marginEnd: 20,
    marginLeft: 20,
    borderRadius: 30,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
      marginBottom: 10
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 16
  },
ListHeader:{
  // height: '10%',
  borderRadius: 30,
  justifyContent: 'center'
   },
   GridColHead: {
    borderColor: BaseColor.BackgroundColor,
    borderWidth: 1,
    justifyContent: 'center',
  },
  GridColBody: {
    borderColor: BaseColor.BackgroundColor,
    borderWidth: 1,
    width: '65%',
  },
  GridColHeadColor: {
    borderColor: BaseColor.BackgroundColor,
    borderWidth: 1,
    justifyContent: 'center',
    backgroundColor: BaseColor.BackgroundColor,
  },
  GridNoColBody: {
    borderColor: BaseColor.BackgroundColor,
    borderWidth: 1,
    width: '50%',
  },
};
