import React from "react-native";

const { StyleSheet } = React;
import BaseColor from './../Core/BaseTheme';

export default {
    LoginInputStyle:{
        borderColor: BaseColor.HeaderColor,
        width: '90%',
        justifyContent: 'center',
        alignSelf: 'center',
      },
    TextAreaStyle:{
       width:'90%',
      
       borderRadius: 5,
        borderColor: BaseColor.HeaderColor,
         marginTop: 10,
          marginBottom: 10 
    }  ,
    checkBox: {
      flexDirection: 'row',
      alignItems: 'center'
  }
 
};
