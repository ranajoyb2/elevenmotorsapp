import React from "react-native";

const { StyleSheet } = React;
import BaseColor from './../Core/BaseTheme';

export default {
  LoginHeader:{
    backgroundColor: BaseColor.HeaderColor,
    height:160,
    borderBottomLeftRadius:25,
    borderBottomRightRadius:25,
    shadowColor: '#000',
    shadowOffset: {
    width: 0,
    height: 8,
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 16,justifyContent:"center",alignItems:'center'
  },
  AddNew:{
    backgroundColor: BaseColor.ColorWhite,
    margin: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,

    elevation: 1,
    height: 50,
    borderRadius:20,
    justifyContent:'center'
  },
  
  DashboardHeader:{
    backgroundColor: BaseColor.HeaderColor,
    height:200,
    borderBottomLeftRadius:90,
    borderBottomRightRadius:90,
    shadowColor: '#000',
    shadowOffset: {
    width: 0,
    height: 8,
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 16,justifyContent:"center",alignItems:'center'
  },
  AllHeader:{
    backgroundColor: BaseColor.HeaderColor,
    height:130,
    borderBottomLeftRadius:90,
    borderBottomRightRadius:90,
    shadowColor: '#000',
    shadowOffset: {
    width: 0,
    height: 8,
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 16,justifyContent:"center",alignItems:'center'
  },
 
};
