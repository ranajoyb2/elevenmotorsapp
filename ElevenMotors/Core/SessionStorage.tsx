
import AsyncStorage from '@react-native-community/async-storage'
import { User } from '../Entity/User';

export default class SessionStorage{
  public static SetSession(AdminUser:User) {
        AsyncStorage.setItem('User', JSON.stringify(AdminUser))
    }
    public static async GetSession() {
        var item = await AsyncStorage.getItem('User');
        if (!item) {
            return item as unknown as User;
        }
        return JSON.parse(item) as User;
    }
}