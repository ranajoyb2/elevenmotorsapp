import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';



import { TouchableOpacity, Text } from 'react-native';
import BaseColor from '../Core/BaseTheme';
import InputStyle from '../Styles/InputStyle';


const CustomCheckBox = ({ selected, onPress, style, textStyle, size = 30, color = BaseColor.HeaderColor, text = '', ...props}) => (
    <TouchableOpacity style={[InputStyle.checkBox, style]} onPress={onPress} {...props}>
        <Icon
            size={size}
            color={color}
            name={ selected ? 'check-box' : 'check-box-outline-blank'}
        />

        <Text style={textStyle}> {text} </Text>
    </TouchableOpacity>
)

export default CustomCheckBox