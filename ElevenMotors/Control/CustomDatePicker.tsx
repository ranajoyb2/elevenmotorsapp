import React from 'react';
import {DatePicker} from 'native-base';
 
import PropTypes from 'prop-types';
import BaseColor from '../Core/BaseTheme';



export default function CustomDatePicker(props) {
    var {onDateChange,placeHolderText,disabled} = props;

    if(!placeHolderText){
        placeHolderText="Select date"
    }

    return (
        <DatePicker
            locale={"en"}
            timeZoneOffsetInMinutes={undefined}
            modalTransparent={false}                
            animationType={"slide"}
            androidMode={"spinner"}
            placeHolderText={placeHolderText}
        
            textStyle={{ color: BaseColor.HeaderColor }}
            placeHolderTextStyle={{ color: "#d3d3d3" }}
            onDateChange={(date) => { onDateChange(date) }}
            disabled={disabled}
      />
    )
}

CustomDatePicker.propTypes = {
    onDateChange:PropTypes.func,
    placeHolderText:PropTypes.string,
    disabled:PropTypes.bool
}