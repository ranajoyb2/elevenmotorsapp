import BaseApi from "../Core/BaseApi";

export default class StudentDataAccess extends BaseApi{
    public static Create(Model:any,callback:any){
        this.BasePostRequest("Student/Add",Model,callback);
    }
    public static Update(Model:any,callback:any){
        this.BasePostRequest("Student/Update",Model,callback);
    }
    public static GetAll(Model:any,callback:any){
        this.BasePostRequest("Student/GetAll",Model,callback);
    }
    public static Get(Model:any,callback:any){
        this.BasePostRequest("Student/GetByStudentId",Model,callback);
    }
    public static GetAllSubject(Model:any,callback:any){
        this.BasePostRequest("Student/GetSubjects",Model,callback);
    }
    public static GetAllGender(Model:any,callback:any){
        this.BasePostRequest("Student/GetGenders",Model,callback);
    }
    public static GetAllClass(Model:any,callback:any){
        this.BasePostRequest("Student/GetClasses",Model,callback);
    }
}