import React, { Component } from 'react';
import { Image, View, SafeAreaView, StatusBar, Modal, Picker, FlatList, TouchableOpacity, ActivityIndicator, StyleSheet } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import BaseColor from '../../Core/BaseTheme'
import {
  Text, Button, Item, Input, Label, Card, Icon, Container, Content, Form, DatePicker, ListItem, Left, Body, CardItem, Grid,
  Row,
  Col,
} from 'native-base';
import AppIconImage from '../../assets/AppIconImage';
import ButtonStyle from '../../Styles/ButtonStyle';
import TextStyle from '../../Styles/TextStyle';
import InputStyle from '../../Styles/InputStyle';
import ViewStyle from '../../Styles/ViewStyle';
import SafeAreaStyle from '../../Styles/SafeAreaStyle';
import CardStyle from '../../Styles/CardStyle';
import DateTimePicker from '@react-native-community/datetimepicker';
import SessionStorage from '../../Core/SessionStorage';
import StudentDataAccess from '../../DataAccess/StudentDataAccess';
import Icon8 from 'react-native-vector-icons/MaterialCommunityIcons';
import { format, parseISO } from 'date-fns';




type State = {
  Dob: any,
  StudentList: any,
  Class: any,
  Document: any[],
  loading: Boolean;
  StudentId: string;
  DocumentUrl: string;
  StudentName: string;
  Roll: any;
  Gender: string;
  RollNo: string;
  SubjectList: any[],

}
export class RequestEntity {
  ApiToken: string = '';
  StudentId: string = '';
}

export default class AllStudentPage extends Component<any, State> {
  constructor(props: any) {
    super(props);

    this.state = this.InitialState();

  }
  InitialState = () => ({
    Roll: '',
    StudentName: '',
    Document: [],
    DocumentUrl: '',
    Dob: new Date(),
    StudentList: '',
    Class: '',
    loading: false,
    StudentId: '',
    Gender: '',
    RollNo: '',
    SubjectList: [],

  })
  componentDidMount() {
    this.LoadStudentList();
  }
  LoadStudentList = async () => {
    this.setState((_state, props) => ({
      StudentId: props.route.params.StudentId
    }));
    var StudentId = this.state.StudentId;
    console.log(StudentId);
    var session = SessionStorage.GetSession();
    var rpojo = new RequestEntity();
    rpojo.ApiToken = (await session).ApiToken;
    rpojo.StudentId = this.state.StudentId;
    console.log(rpojo);
    this.setState({ loading: true })
    StudentDataAccess.Get(rpojo, (res: any) => {
      this.setState({ loading: false })
      if (!res) {
        return;
      }
      this.setState({ StudentList: res.Data })
      console.log(this.state.StudentList);
      this.setState({ StudentName: this.state.StudentList.Name })
      this.setState({ Dob: new Date(this.state.StudentList.DOB) })
      this.setState({ Gender: this.state.StudentList.Gender })
      this.setState({ Class: this.state.StudentList.Class })
      this.setState({ RollNo: this.state.StudentList.RollNo })
      this.setState({ SubjectList: this.state.StudentList.Subjects })
      this.setState({ Document: this.state.StudentList.Documents })


      //  alert(this.state.countrylist);
    })

  }



  render() {
    console.log(this.state.StudentId);
    return (
      <Container>

        <View style={ViewStyle.FullScreenView}>
          <StatusBar
            barStyle="dark-content"
            hidden={false}
            backgroundColor={BaseColor.HeaderColor}
            translucent={true}
          />


          <SafeAreaView style={{ alignItems: 'center', marginTop: 50 }}>
            <CardItem header style={{ height: 120 }}>
              <Icon8
                name="account-circle"
                size={80}
                color={BaseColor.HeaderColor}
                style={{ alignSelf: 'center', textAlign: 'center' }}
              />

            </CardItem>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text>{this.state.StudentName}</Text>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate({
                    name: 'AddStudentPage',
                    params: {
                      StudentId: this.state.StudentId,
                    },
                  });
                }}>
                <Icon8
                  name="pencil"
                  size={21}
                  color={BaseColor.HeaderColor}
                  style={{ alignSelf: 'center', textAlign: 'center', marginLeft: 10 }}
                />
              </TouchableOpacity>
            </View>
            <CardItem footer>

            </CardItem>
          </SafeAreaView>
          <SafeAreaView style={{ margin: 10 }}>

          </SafeAreaView>
          <Content>
            <Grid>
              <Row>
                <Col style={ViewStyle.GridColHeadColor}>
                  <Text style={TextStyle.GridBodyTxt}>Date of birth:</Text>
                </Col>
                <Col style={ViewStyle.GridNoColBody}>
                  <ListItem>
                    <Text style={TextStyle.GridBodyTxt}>
                      {format(this.state.Dob, "dd-MMM-yyyy")}
                      {/* { format(parseISO(this.state.Dob.toString()), 'yyyy-MM-dd')} */}
                      {/* {this.state.Dob} */}
                    </Text>
                  </ListItem>
                </Col>
              </Row>
              <Row>
                <Col style={ViewStyle.GridColHeadColor}>
                  <Text style={TextStyle.GridBodyTxt}>Gender:</Text>
                </Col>
                <Col style={ViewStyle.GridNoColBody}>
                  <ListItem>
                    <Text style={TextStyle.GridBodyTxt}>
                      {this.state.Gender}
                    </Text>
                  </ListItem>
                </Col>
              </Row>
              <Row>
                <Col style={ViewStyle.GridColHeadColor}>
                  <Text style={TextStyle.GridBodyTxt}>Class:</Text>
                </Col>
                <Col style={ViewStyle.GridNoColBody}>
                  <ListItem>
                    <Text style={TextStyle.GridBodyTxt}>
                      {this.state.Class.Name}
                    </Text>
                  </ListItem>
                </Col>
              </Row>
              <Row>
                <Col style={ViewStyle.GridColHeadColor}>
                  <Text style={TextStyle.GridBodyTxt}>Roll No:</Text>
                </Col>
                <Col style={ViewStyle.GridNoColBody}>
                  <ListItem>
                    <Text style={TextStyle.GridBodyTxt}>
                      {this.state.RollNo}
                    </Text>
                  </ListItem>
                </Col>
              </Row>

              <Row>
                <Col style={ViewStyle.GridColHeadColor}>
                  <Text style={TextStyle.GridBodyTxt}>Suject:</Text>
                </Col>
                <Col style={ViewStyle.GridNoColBody}>
                  <FlatList
                    data={this.state.SubjectList}
                    extraData={this.state}
                    keyExtractor={(item) => item.SubjectId}
                    scrollEnabled={true}
                    renderItem={({ item }) => {
                      var i = item;
                      return (
                        <ListItem>
                          <Text style={TextStyle.GridBodyTxt}>
                            {i.Name}
                          </Text>

                          {/* <Text style={TextStyle.GridBodyTxt}>
                                Quantity:{i.Quantity}
                              </Text> */}
                        </ListItem>
                      );
                    }}
                  />
                </Col>
              </Row>


            </Grid>
            <Text style={{ alignSelf: 'center', color: BaseColor.HeaderColor }}>
              Images
            </Text>
            <FlatList
              data={this.state.Document}
              horizontal={true}
              extraData={this.state}
              keyExtractor={(item) => item.SubjectId}
              scrollEnabled={true}
              renderItem={({ item }) => {
                var i = item;
                return (
                  <View style={{ margin: 10 }}>

                    <Image source={{ uri: i.DocumentUrl }}
                      style={{ width: 150, height: 150 }}
                    />
                  </View>
                );
              }}
            />
          </Content>

        </View>
        <Modal animationType="fade" transparent={true} visible={this.state.loading}>
          <View style={styles.modalContainer}></View>
          <View style={styles.activityIndicator}>

            <ActivityIndicator animating={true} size="large" color={BaseColor.HeaderColor} />

          </View>
        </Modal>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  activityIndicator: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#000',
    opacity: 0.2,
  },
});
