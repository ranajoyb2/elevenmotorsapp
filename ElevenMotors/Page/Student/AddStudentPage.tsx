import React, { Component } from 'react';
import { View, SafeAreaView, StatusBar, Modal, Picker, FlatList, TouchableOpacity, ActivityIndicator,StyleSheet } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import BaseColor from '../../Core/BaseTheme'
import { Text, Button, Item, Input, Label, Card, Icon, Container, Content, Form, DatePicker, ListItem, Toast } from 'native-base';
import AppIconImage from '../../assets/AppIconImage';
import ButtonStyle from '../../Styles/ButtonStyle';
import TextStyle from '../../Styles/TextStyle';
import InputStyle from '../../Styles/InputStyle';
import ViewStyle from '../../Styles/ViewStyle';
import SafeAreaStyle from '../../Styles/SafeAreaStyle';
import CardStyle from '../../Styles/CardStyle';
import DateTimePicker, { Event } from '@react-native-community/datetimepicker';
import CustomDatePicker from '../../Control/CustomDatePicker';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import { parseISO, format } from "date-fns";
import MultiSelect from 'react-native-multiple-select';
import SearchableDropDown from '../../Control/SearchableDropDown';
import { Subject } from '../../Entity/User';
import SessionStorage from '../../Core/SessionStorage';
import StudentDataAccess from '../../DataAccess/StudentDataAccess';
import CustomCheckBox from '../../Control/CustomCheckBox';
export class SearchableDropDownEntity {
  id: string = "";
  name: string = "";
}
export class RequestEntity {
  ApiToken: string = '';
  StudentId: string = '';
}
export class StudentCreateRequestEntity {
  Name: string='';
  Gender: string='';
  ClassId: string='';
  DOB?: Date;
  IsSpeciallyEligible?: boolean;
  Subjects: string[]=[];
  ApiToken: string='';
  StudentId:string='';
}
const Data = [
  {
    item: 'male',
    value: 'male'
  },
  {
    item: 'Female',
    value: 'Female'
  }
]

type State = {
  Dob: any,
  Show: Boolean;
  Label: string,
  Gender: string,
  ClassId: string,
  Name: string,
  SubjectList: Subject[],
  SubjectSelectedList: Subject[],

  SubjectAddList: Subject[],
  loading: Boolean;
  AccidentalRider: boolean;
  GenderList: any[],
  ClassList:any[],
  StudentId:string;
  StudentList: any,
  ButtonText:string;
}


export default class AddStudentPage extends Component<any, State> {
  constructor(props: any) {
    super(props);
    this.state = this.InitialState();
  }
  InitialState = () => ({
    // Dob: new Date(),
    Dob: new Date(),
    Show: false,
    Label: '',
    SubjectList: [],
    GenderList: [],
    SubjectSelectedList: [],
    loading: false,
    SubjectAddList: [],
    ClassList:[],
    AccidentalRider: false,
    Gender: '',
    Name: '',
    ClassId: '',
    StudentId:'',
    StudentList: '',
    ButtonText:'Create',

  })
  componentDidMount() {
    this.LoadStudentList();
    this.LoadGenderList();
    this.LoadClassList();
    this.LoadStudentDetailsList();

  }
  LoadStudentDetailsList = async () => {
    if(!this.props.route.params.StudentId){
      return;
    }
    this.setState((_state, props) => ({
      StudentId:props.route.params.StudentId
    }));
   var StudentId=this.state.StudentId;
   this.setState({ ButtonText:'Update'})
   var session=  SessionStorage.GetSession();
   var rpojo = new RequestEntity();
   rpojo.ApiToken = (await session).ApiToken;
   rpojo.StudentId=this.state.StudentId;
   console.log(rpojo);
   this.setState({ loading: true })
   StudentDataAccess.Get(rpojo, (res: any) => {
       this.setState({ loading: false })
       if (!res) {
           return;
       }
       this.setState({ StudentList: res.Data })
       console.log(this.state.StudentList);
       this.setState({ Name:this.state.StudentList.Name })
       this.setState({ Dob: new Date(this.state.StudentList.DOB)})
       this.setState({ Label: format(this.state.Dob, "dd-MMM-yyyy") });
       this.setState({ Gender:this.state.StudentList.Gender})
       this.setState({ ClassId:this.state.StudentList.ClassId })
       this.setState({AccidentalRider:this.state.StudentList.IsSpeciallyEligible})
       this.setState({SubjectSelectedList:this.state.StudentList.Subjects})
      //  this.setState({ Document:this.state.StudentList.Documents })
  
      
       //  alert(this.state.countrylist);
   })
  
  }
  LoadGenderList = async () => {

    var rpojo = new RequestEntity();
 
  
    this.setState({ loading: true })
    StudentDataAccess.GetAllGender(rpojo, (res: any) => {
      this.setState({ loading: false })
      if (!res) {
        return;
      }
      this.setState({ GenderList: res.Data })
      console.log(this.state.GenderList);
      //  alert(this.state.countrylist);
    })
  }
  LoadClassList = async () => {
    var session = SessionStorage.GetSession();
    var rpojo = new RequestEntity();
    rpojo.ApiToken = (await session).ApiToken;
    console.log(rpojo);
    this.setState({ loading: true })
    StudentDataAccess.GetAllClass(rpojo, (res: any) => {
      this.setState({ loading: false })
      if (!res) {
        return;
      }
      this.setState({ ClassList: res.Data })
      console.log(this.state.ClassList);
      //  alert(this.state.countrylist);
    })
  }
  LoadStudentList = async () => {
    var session = SessionStorage.GetSession();
    var rpojo = new RequestEntity();
    rpojo.ApiToken = (await session).ApiToken;
    console.log(rpojo);
    this.setState({ loading: true })
    StudentDataAccess.GetAllSubject(rpojo, (res: any) => {
      this.setState({ loading: false })
      if (!res) {
        return;
      }
      this.setState({ SubjectList: res.Data })
      // console.log(this.state.SubjectList);
      //  alert(this.state.countrylist);
    })
  }

  setDate(date) {
    this.setState({ Dob: new Date(date) });
  }
  setDOBData = (event: Event, selectedDate: Date) => {
    this.setState({ Show: false })
    this.setState({ Dob: new Date(selectedDate) });
    this.setState({ Label: format(selectedDate, "dd-MMM-yyyy") });


  }

  onSelectedItemsChange = (event: Event, selectedDate: Date) => {
    this.setState({ Show: false })
    this.setState({ Dob: new Date(selectedDate) });
    this.setState({ Label: format(selectedDate, "dd-MMM-yyyy") });


  }
  handlelogin = async () => {
    console.log(this.state.ClassId)
    var session = SessionStorage.GetSession();
    var rpojo = new StudentCreateRequestEntity();
    rpojo.ApiToken = (await session).ApiToken;
    rpojo.Gender=this.state.Gender;
    rpojo.ClassId=this.state.ClassId;
    rpojo.StudentId=this.state.StudentId;
    rpojo.DOB=this.state.Dob;
    rpojo.IsSpeciallyEligible=this.state.AccidentalRider;
    rpojo.Name=this.state.Name;
    rpojo.Subjects=this.state.SubjectSelectedList.map(i => i.SubjectId);
    console.log(rpojo);
    this.setState({ loading: true })
    if (!rpojo.StudentId) {
    StudentDataAccess.Create(rpojo, (res: any) => {
      this.setState({ loading: false })
      if (!res) {
        Toast.show({
          text: res.Message,
          type: 'danger',
          duration: 2000,
        });
        return;
      }
      console.log(res.Data);
      Toast.show({
        text: res.Message,
        type: 'danger',
        duration: 2000,
      });
      this.props.navigation.navigate('DashboardPage');
    })
  }
  else {

    StudentDataAccess.Update(rpojo, (res: any) => {
      this.setState({ loading: false })
      if (!res) {
        Toast.show({
          text: res.Message,
          type: 'danger',
          duration: 2000,
        });
        return;
      }
      console.log(res.Data);
      Toast.show({
        text: res.Message,
        type: 'danger',
        duration: 2000,
      });
      this.props.navigation.navigate('DashboardPage');
    })
  }

  }

  ShowPicker = async () => {
    this.setState({ Show: true })
    // this.props.navigation.navigate('DashboardPage');


  }
  handleAccidentalRider = () => {
    this.setState({AccidentalRider: !this.state.AccidentalRider});
    console.log(this.state.AccidentalRider)
  };
  render() {
    console.log('KKK',this.state.StudentId);
    var SubjectSelectedPickerList = this.state.SubjectSelectedList.map((s, i) => { return { id: s.SubjectId, name: s.Name } })
    var SubjectPickerList = this.state.SubjectList.map((s, i) => { return { id: s.SubjectId, name: s.Name } })
    var FinancialYearList = this.state.GenderList.map((i, k) => {
      return (
        <Picker.Item
          label={i.Name}
          key={k}
          value={i.Value}
        />
      );
    });
    var ClassList = this.state.ClassList.map((i, k) => {
      return (
        <Picker.Item
          label={i.Name}
          key={k}
          value={i.ClassId}
        />
      );
    });
    return (
      <Container>
        <StatusBar barStyle="dark-content" hidden={false} backgroundColor={BaseColor.ColorWhite} translucent={true} />
        <Content style={{ marginTop: '25%' }}>

          <Item floatingLabel style={{ width: '80%', alignSelf: 'center' }}>
            <Icon active name='person-circle' style={{ color: BaseColor.HeaderColor }} />
            <Label style={TextStyle.InputLabelStyle}>
              Enter Name
            </Label>

            <Input
            value={this.state.Name}
                onChangeText={Name=> this.setState({ Name })}
              autoCapitalize="none"
              style={InputStyle.LoginInputStyle}
            />
          </Item>
          <Label style={{ width: '80%', color: BaseColor.HeaderColor, alignSelf: 'center', marginTop: 15 }}>
            Select Gender
          </Label>

          <Picker
            mode="dialog"
            selectedValue={this.state.Gender}
            style={{ width: '80%', alignSelf: 'center', }}
          onValueChange={Gender=> this.setState({ Gender})}
          >
            {FinancialYearList}

          </Picker>
          <Label style={{ width: '80%', color: BaseColor.HeaderColor, alignSelf: 'center', marginTop: 15 }}>
            Select Class
          </Label>

          <Picker
            mode="dialog"
            selectedValue={this.state.ClassId}
            style={{ width: '80%', alignSelf: 'center', }}
            onValueChange={ClassId=> this.setState({ ClassId})}
          >
            {ClassList}

          </Picker>
{/* 
          <Item
            floatingLabel
            style={{ width: '80%', alignSelf: 'center', marginTop: 15 }}>
            <Icon active name='key-sharp' style={{ color: BaseColor.HeaderColor }} />
            <Label style={TextStyle.InputLabelStyle}>
              Enter class
            </Label>
            <Input
              // onChangeText={(text) => this.SetModelValue('Password', text)}
              autoCapitalize="none"
              secureTextEntry={true}
              style={InputStyle.LoginInputStyle}
            />
          </Item> */}
          <Item
            style={{ width: '80%', alignSelf: 'center', marginTop: 15 }}
            stackedLabel
            disabled={false}
          >
            <Label style={TextStyle.InputLabelStyle}>Select Birthday</Label>
            <View style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
              <Input disabled={true} style={{ margin: 0, padding: 0 }} value={this.state.Label} />
              <MaterialCommunityIcon onPress={this.ShowPicker} style={{ fontSize: 25, marginEnd: 10 }} color={BaseColor.HeaderColor} name={"timetable"} />

            </View>
            {this.state.Show && (
              <DateTimePicker
                testID="dateTimePicker"
                value={this.state.Dob}
                mode='date'
                placeholderText="Select Date"
                is24Hour={true}
                display="default"
                onChange={((event, selectedDate) => this.setDOBData(event, selectedDate))} />
            )}

          </Item>
          <SafeAreaView style={{ width: '80%', alignSelf: 'center', marginTop: 15 }}>
            <Label style={TextStyle.InputLabelStyle}>Select Subjects</Label>

            <SearchableDropDown
              multi={true}
              selectedItems={SubjectSelectedPickerList}
              onItemSelect={(item: SearchableDropDownEntity) => {

                var tempSubject = this.state.SubjectList.find((sitem) => sitem.SubjectId === item.id);
                this.state.SubjectAddList.push(tempSubject)
                this.setState({ SubjectSelectedList: this.state.SubjectAddList });
                console.log(this.state.SubjectSelectedList)


              }}
              containerStyle={{ padding: 5 }}
              onRemoveItem={(item: SearchableDropDownEntity, index) => {
                const items = this.state.SubjectSelectedList.filter((sitem) => sitem.SubjectId !== item.id);
                console.log(items)

                this.setState({ SubjectSelectedList: items });
              }}
              itemStyle={{
                padding: 10,
                marginTop: 2,
                backgroundColor: BaseColor.BackgroundColor,
                borderColor: BaseColor.HeaderColor,
                borderWidth: 1,
                borderRadius: 5,
              }}
              itemTextStyle={{ color: '#222' }}
              itemsContainerStyle={{ maxHeight: 140 }}
              items={SubjectPickerList}
              defaultIndex={2}
              chip={true}
              resetValue={false}
              textInputProps={
                {
                  placeholder: "Select subjects",
                  underlineColorAndroid: "transparent",
                  style: {

                    padding: 12,
                    borderWidth: 1,
                    borderColor: BaseColor.HeaderColor,
                    borderRadius: 5,

                  },
                  onTextChange: text => console.log(text)
                }
              }
              listProps={
                {
                  nestedScrollEnabled: true,
                }
              }
            />

          </SafeAreaView>
          <ListItem>
                      <CustomCheckBox
                        selected={this.state.AccidentalRider}
                        onPress={this.handleAccidentalRider}
                        text="Is SpeciallyEligible"
                      />
                    </ListItem>
          <Button onPress={this.handlelogin} style={ButtonStyle.SimpleBtnStyle}>
            <Text style={TextStyle.SimpleBtnTxt}>
            {this.state.ButtonText}
            </Text>
          </Button>
        </Content>
        <Modal animationType="fade" transparent={true} visible={this.state.loading}>
    <View style={styles.modalContainer}></View>
    <View style={styles.activityIndicator}>
  
          <ActivityIndicator animating={true} size="large" color={BaseColor.HeaderColor} />
     
    </View>
  </Modal>
      </Container>




    );
  }
}
const styles = StyleSheet.create({
  activityIndicator: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#000',
    opacity: 0.2,
  },
});