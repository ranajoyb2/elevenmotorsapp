import React, { Component } from 'react';
import { View, SafeAreaView, StatusBar, Modal, Picker, FlatList, TouchableOpacity, ActivityIndicator,StyleSheet } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import BaseColor from '../../Core/BaseTheme'
import { Text, Button, Item, Input, Label, Card ,Icon, Container, Content,  Form, DatePicker, ListItem, Left, Body} from 'native-base';
import AppIconImage from '../../assets/AppIconImage';
import ButtonStyle from '../../Styles/ButtonStyle';
import TextStyle from '../../Styles/TextStyle';
import InputStyle from '../../Styles/InputStyle';
import ViewStyle from '../../Styles/ViewStyle';
import SafeAreaStyle from '../../Styles/SafeAreaStyle';
import CardStyle from '../../Styles/CardStyle';
import DateTimePicker from '@react-native-community/datetimepicker';
import SessionStorage from '../../Core/SessionStorage';
import StudentDataAccess from '../../DataAccess/StudentDataAccess';




type State = {
  Dob: any,
  StudentList: any[],
  loading:Boolean;
}
export class RequestEntity {
    ApiToken: string = '';
}

export default class AllStudentPage extends Component<any, State> {
  constructor(props: any) {
      super(props);
      this.state = this.InitialState();
  }
  InitialState = () => ({
    Dob: '',
    StudentList: [],
     loading:false,
  })
  componentDidMount() {
    this.LoadStudentList();

}
LoadStudentList = async () => {
    var session=  SessionStorage.GetSession();
    var rpojo = new RequestEntity();
    rpojo.ApiToken = (await session).ApiToken;
    console.log(rpojo);
    this.setState({ loading: true })
    StudentDataAccess.GetAll(rpojo, (res: any) => {
        this.setState({ loading: false })
        if (!res) {
            return;
        }
        this.setState({ StudentList: res.Data })
        console.log(this.state.StudentList);
        //  alert(this.state.countrylist);
    })
}


  render() {
 
    return (
        <Container>
             <View style={{ height: '100%', backgroundColor: BaseColor.ColorWhite }}>
        <StatusBar barStyle="dark-content" hidden={false} backgroundColor={BaseColor.ColorWhite} translucent={true} />
        <View style={ViewStyle.ModalView}>

<FlatList
              data={this.state.StudentList}
              extraData={this.state}
              keyExtractor={(item) => item.StudentId}
              scrollEnabled={true}
             
              renderItem={({item}) => {
                var i = item;
                return (
                    <ListItem avatar iconLeft>
                    <Left>
                  <Icon active name='person-circle' style={{ color: BaseColor.HeaderColor,fontSize: 60, width: 65, height: 65,}} />
                      {/* </Button> */}
                    </Left>
                    <TouchableOpacity
                      onPress={() => {
                        this.props.navigation.navigate({
                          name: 'StudentDetailsPage',
                          params: {
                            StudentId:i.StudentId,
                          },
                        });
                      }}>
                      <Body>
                        <Text
                          style={{ color: BaseColor.HeaderColor, fontSize: 15 }}>
                          {'Name :'+i.Name}
                        </Text>
                        <Text
                          style={{ color: BaseColor.HeaderColor, fontSize: 12 }}>
                          {'Roll No :'+i.RollNo}
                        </Text>
                        <Text
                          style={{ color: BaseColor.HeaderColor, fontSize: 12 }}>
                          {'Class :'+ i.Class.Name}
                        </Text>
                        <Text
                          style={{ color: BaseColor.HeaderColor, fontSize: 12 }}>
                          {'Gender :'+i.Gender}
                        </Text>
                      </Body>
                    </TouchableOpacity>
                  </ListItem>
                    );
                }}
              />
         </View>
         <Modal animationType="fade" transparent={true} visible={this.state.loading}>
    <View style={styles.modalContainer}></View>
    <View style={styles.activityIndicator}>
  
          <ActivityIndicator animating={true} size="large" color={BaseColor.HeaderColor} />
     
    </View>
  </Modal>
         </View>
    </Container>


     

    );
  }
}
const styles = StyleSheet.create({
    activityIndicator: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      justifyContent: 'center',
      alignItems: 'center',
    },
    modalContainer: {
      flex: 1,
      justifyContent: 'center',
      backgroundColor: '#000',
      opacity: 0.2,
    },
  });
