import React, { Component } from 'react';
import { View, SafeAreaView, StatusBar, Modal, ActivityIndicator,StyleSheet } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import BaseColor from '../../Core/BaseTheme'
import { Text, Button, Item, Input, Label, Card, Icon, Toast, Spinner } from 'native-base';
import AppIconImage from '../../assets/AppIconImage';
import ButtonStyle from '../../Styles/ButtonStyle';
import TextStyle from '../../Styles/TextStyle';
import InputStyle from '../../Styles/InputStyle';
import ViewStyle from '../../Styles/ViewStyle';
import SafeAreaStyle from '../../Styles/SafeAreaStyle';
import CardStyle from '../../Styles/CardStyle';
import LoginDataAccess from '../../DataAccess/LoginDataAccess';
import SessionStorage from '../../Core/SessionStorage';
import { User } from '../../Entity/User';





type State = {
  LoginId: String,
  Password: String,
  loading:Boolean;
 
}
export class LoginrequestEntity {
  LoginId: String = '';
  Password: String = '';
  User:any;
 
 
}

export default class LoginPage extends Component<any, State> {
  constructor(props: any) {
    super(props);
    this.state = this.InitialState();
  }
  InitialState = () => ({
    LoginId: '',
    Password: '',
    loading:false,

  })
  componentDidMount() {
    this.CheckIfUserAlreadyLogin();
  }
  CheckIfUserAlreadyLogin = async () => {
    var value = await  SessionStorage.GetSession();

    if (value) {
      // this.ShowToastMesage('You are already logged in', 'warning', 2000);
      this.props.navigation.navigate('DashboardPage');
    }
  };

  handlelogin = async () => {
    var rpojo = new LoginrequestEntity();
    rpojo.LoginId = this.state.LoginId;
    rpojo.Password = this.state.Password;

    console.log(rpojo);
    this.setState({ loading: true })
    LoginDataAccess.Login(rpojo, async (res: any) => {
      this.setState({ loading: false })
      if (!res.IsSucess) {
        console.log(res)
        Toast.show({
          text: res.Message,
          type: 'danger',
          duration: 2000,
        });
       
        return;
      }
      console.log(res.Data);
      
      SessionStorage.SetSession(res.Data as User);
      var session=  SessionStorage.GetSession();
    
          console.log((await session).Name);
          Toast.show({
            text: 'Welcome'+' '+res.Data.Name,
            type: 'success',
            duration: 2000,
          });
      this.props.navigation.navigate('DashboardPage');
    });
  }
  render() {
    console.log(this.state.Password);
    return (
      <View style={{ height: '100%', backgroundColor: BaseColor.ColorWhite }}>
        <StatusBar barStyle="dark-content" hidden={false} backgroundColor={BaseColor.ColorWhite} translucent={true} />
       
        {/* 
        <SafeAreaView style={SafeAreaStyle.LoginHeader}>
          <Card style={CardStyle.LoginIconCard}>
            <AppIconImage />
          </Card>
        </SafeAreaView> */}
 {/* <Spinner color="red" /> */}
        <View style={ViewStyle.ModalView}>
          <Text style={TextStyle.LoginTxt}>LOGIN</Text>
          <SafeAreaView style={{ marginTop: 25 }}>
            <Item floatingLabel style={{ width: '80%', alignSelf: 'center' }}>
              <Icon active name='person-circle' style={{ color: BaseColor.HeaderColor }} />
              <Label style={TextStyle.InputLabelStyle}>
                Enter your Username
              </Label>

              <Input
                //  name="LoginId"
                //  onChange={(e) => this.handleInputChange(e)}
                //  value={this.state.LoginId}
                onChangeText={LoginId => this.setState({ LoginId })}
                autoCapitalize="none"
                style={InputStyle.LoginInputStyle}
              />
            </Item>
            <Item
              floatingLabel
              style={{ width: '80%', alignSelf: 'center', marginTop: 15 }}>
              <Icon active name='key-sharp' style={{ color: BaseColor.HeaderColor }} />
              <Label style={TextStyle.InputLabelStyle}>
                Password
              </Label>
              <Input

                onChangeText={Password => this.setState({ Password })}

                autoCapitalize="none"
                secureTextEntry={true}

                style={InputStyle.LoginInputStyle}
              />
            </Item>
            <Button onPress={this.handlelogin} style={ButtonStyle.SimpleBtnStyle}>
              <Text style={TextStyle.SimpleBtnTxt}>
                Login
              </Text>
            </Button>
          </SafeAreaView>
          
        {/* <View style={{ position: 'absolute', top: 0, bottom: 0, right: 0, left: 0 }}>
              <ActivityIndicator animating={true} size="small" color="#00ff00" />
            </View> */}
        </View>
        <Modal animationType="fade" transparent={true} visible={this.state.loading}>
    <View style={styles.modalContainer}></View>
    <View style={styles.activityIndicator}>
  
          <ActivityIndicator animating={true} size="large" color={BaseColor.HeaderColor} />
     
    </View>
  </Modal>
      </View>

    );
  }
}
const styles = StyleSheet.create({
  activityIndicator: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#000',
    opacity: 0.2,
  },
});