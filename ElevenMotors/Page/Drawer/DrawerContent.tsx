import React from 'react';
import { Alert, Linking, StyleSheet } from 'react-native';
import {
    DrawerContentScrollView,
    DrawerItem,
    DrawerItemList,
} from '@react-navigation/drawer';
import Icon7 from 'react-native-vector-icons/Ionicons';

import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Icon6 from 'react-native-vector-icons/Entypo';
import Icon8 from 'react-native-vector-icons/MaterialCommunityIcons';


import { Separator, Text, Toast, View } from 'native-base';

import Icon2 from 'react-native-vector-icons/Octicons';
import BaseColor from '../../Core/BaseTheme';
import SessionStorage from '../../Core/SessionStorage';



function DrawerContent(props: any) {
    const logout = async () => {
        Alert.alert(
            'Logout',
            'Do you want to logout',
            [
              {text: 'NO', onPress: () => { props.navigation.navigate('DashboardPage')}},
              {text: 'YES', onPress: () => {
                SessionStorage.SetSession(null);
                Toast.show({
                  text: 'You have logged out sucesfully',
                  type: 'success',
                  duration: 2000,
                });
                props.navigation.navigate('LoginPage')
              }},
            ]
          );
     
            // setCustomer(null);
    };

    return (

        <DrawerContentScrollView {...props} style={{ backgroundColor: BaseColor.HeaderColor }}>
            <DrawerItemList {...props} />

            <DrawerItem style={style.DrawerItem}
                label="Add Student"
                labelStyle={style.label}
                icon={() => <FontAwesome name="user-circle-o" style={style.Icon} />}
                onPress={() => { props.navigation.navigate('AddStudentPage') }}
            />
            <DrawerItem style={style.DrawerItem}
                label="All Student"
                labelStyle={style.label}
                icon={() => <FontAwesome5 name="calendar-alt" style={style.Icon} />}
                onPress={() => { props.navigation.navigate('AllStudentPage') }}
            />
            {/* <DrawerItem style={style.DrawerItem}
                label="Loan"
                labelStyle={style.label}
                icon={() => <FontAwesome5 name="donate" style={style.Icon} />}
                onPress={() => { props.navigation.navigate('LoanDetailPage') }}
            /> */}

            {/* <DrawerItem style={style.DrawerItem}
                label="Leave"
                labelStyle={style.label}
                icon={() => <FontAwesome5 name="user-minus" style={style.Icon} />}
                onPress={() => { props.navigation.navigate('LeaveDetailsPage') }}
            />
            <DrawerItem style={style.DrawerItem}
                label="Attendence"
                labelStyle={style.label}
                icon={() => <FontAwesome5 name="calendar-alt" style={style.Icon} />}
                onPress={() => { props.navigation.navigate('AttendencePage') }}
            />
            <DrawerItem style={style.DrawerItem}
                label="Daily Attendence"
                labelStyle={style.label}
                icon={() => <FontAwesome5 name="hands" style={style.Icon} />}
                onPress={() => { props.navigation.navigate('DailyAttendencePage') }}
            /> */}
            <DrawerItem
                label="Log out"
                labelStyle={style.label}
                icon={() => <FontAwesome name="sign-out" style={style.Icon} />}
                onPress={logout}
            />
        </DrawerContentScrollView>

    );
}

export default DrawerContent;

const style = StyleSheet.create({
    DrawerItem: {

    },
    label: {
        color: BaseColor.ColorWhite,
        fontWeight: 'bold',
        alignSelf: "flex-start"
    },
    Icon: {
        color: BaseColor.ColorWhite,
        marginTop: 5,
        fontSize: 25,
        alignSelf: "flex-start",
        width: 30
    },
});