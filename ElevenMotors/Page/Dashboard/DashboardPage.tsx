import React, { Component } from 'react';
import { View, SafeAreaView, StatusBar, Modal } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import BaseColor from '../../Core/BaseTheme'
import {Container, Footer, Title, Button, Icon, Content, FooterTab,Text} from 'native-base';






export default class DashboardPage extends Component {
  constructor(props: any) {
    super(props);

  }

  handleAddStudemt = async () => {
 this.props.navigation.navigate('AddStudentPage');
}
handleAllStudemt = async () => {
  this.props.navigation.navigate('AllStudentPage');
 }
  render() {

    return (
      <Container>
        <StatusBar barStyle="dark-content" hidden={false} backgroundColor={BaseColor.HeaderColor} translucent={true} />
<Content></Content>
<Footer style={{backgroundColor:BaseColor.HeaderColor}}>
          <FooterTab style={{backgroundColor:BaseColor.HeaderColor}}>
            <Button onPress={this.handleAllStudemt}>
            <Icon name="ios-people" style={{ color: BaseColor.ColorWhite}} />
              <Text style={{color:BaseColor.ColorWhite}}>All student</Text>
            </Button>
            <Button onPress={this.handleAddStudemt}>
            <Icon name="person-add" style={{ color: BaseColor.ColorWhite}}/>
              <Text style={{color:BaseColor.ColorWhite}}>Create student</Text>
            </Button>
          
          </FooterTab>
        </Footer>
      </Container>

    );
  }
}
