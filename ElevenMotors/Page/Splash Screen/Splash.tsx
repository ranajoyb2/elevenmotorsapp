import React, { Component } from 'react';
import { View, StatusBar, ActivityIndicator, ImageBackground, SafeAreaView } from 'react-native';
import AppIconImage from '../../assets/AppIconImage';

import BaseColor from '../../Core/BaseTheme';


import { Container, Spinner } from 'native-base';


export default class Splash extends Component<any, any>{

    constructor(props) {
        super(props);
        setTimeout(() => {
            this.props.navigation.reset({
                index: 0,
                routes: [{ name: 'LoginPage' }],
            });
        }, 10000);
    }
    render() {
        return (
            <Container>
            <StatusBar
                barStyle="dark-content"
                hidden={true}
                backgroundColor={BaseColor.ColorWhite}
                translucent={true}
            />
            <ImageBackground
                source={require('../../assets/splash.png')}
                style={{
                    flex: 1,
                    width: '100%',
                    height: '100%',
                     resizeMode: 'cover',
                    //  justifyContent: 'center',
                  }}>
                <SafeAreaView style={{ marginTop: '80%' }}>
                <ActivityIndicator animating={true} size="large" color={BaseColor.ColorWhite} />
                </SafeAreaView>
            
            </ImageBackground>
        </Container>
        );
    }
}